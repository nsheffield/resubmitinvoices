﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.ServiceModel;
using Microsoft.Office.Interop.Excel;

namespace BTAPIConsoleAppBase
{
    class Program
    {
        private static invService.InvoiceServiceClient CreateInvoiceServiceClient()
        {
            

            BasicHttpBinding myBinding = new BasicHttpBinding();
            myBinding.Security.Mode = BasicHttpSecurityMode.Transport;
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress ea = new EndpointAddress("https://webservices.brightree.net/v0100-1307/InvoiceService/InvoiceService.svc");
            invService.InvoiceServiceClient client = new invService.InvoiceServiceClient();
            client.ClientCredentials.UserName.UserName = PublicVars.UserID;
            client.ClientCredentials.UserName.Password = PublicVars.UserPassword;
            return client;
        }

        static void Main(string[] args)
        {
             Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();

            if (xlApp == null)
            {
                Console.WriteLine("Excel could not be started. Check your installation and project references.");
            }
            xlApp.Visible = true;

            Workbook wb = xlApp.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
            Worksheet ws = (Worksheet)wb.Worksheets[1];
            var activesheet = wb.ActiveSheet;

            if (ws == null)
            {
                Console.WriteLine("Worksheet could not be created.  Check installation and project references.");
            }

            //List<int> Invoices = new List<int>();
            int[] Invoices = {
            3609617 ,
            3610517 ,

//3610520 ,
//3610523 ,
//3616489 ,
//3617796 ,
//3617878 ,
//3617983 ,
//3618512 ,
//3619090 ,
//3619269 ,
//3623282 ,
//3623353 ,
//3624237 ,
//3624286 ,
//3624501 ,
//3624570 ,
//3624897 ,
//3626458 ,
//3626973 ,
//3628084 ,
//3628283 ,
//3629083 ,
//3630174 ,
//3630177 ,
//3631054 ,
//3631200 ,
//3632202 ,
//3632428 ,
//3634645 ,
//3636508 ,
//3636547 ,
//3638417 ,
//3638736 ,
//3640376 ,
//3640422 ,
//3640957 ,
//3642135 ,
//3643646 ,
//3644882 ,
//3645143 ,
//3645724 ,
//3647312 ,
//3647357 ,
//3647829 ,
//3648167 ,
//3648524 ,
//3648540 ,
//3648820 ,
//3648940 ,
//3649157 ,
//3649211 ,
//3649281 ,
//3649723 ,
//3650156 ,
//3650983 ,
//3651271 ,
//3651474 ,
//3652183 ,
//3653559 ,
//3653857 ,
//3653968 ,
//3657079 ,
//3657122 ,
//3657290 ,
//3658216 ,
//3658273 ,
//3658352 ,
//3658440 ,
//3658691 ,
//3658697 ,
//3658700 ,
//3659912 ,
//3660840 ,
//3660934 ,
//3661131 ,
//3664884 ,
//3665847 ,
//3666538 ,
//3668046 ,
//3668118 ,
//3669867 ,
//3669871 ,
//3671748 ,
//3671751 ,
//3672745 ,
//3673879 ,
//3678262 ,
//3679059 ,
//3679357 ,
//3680336 ,
//3680390 ,
//3684439 ,
//3685401 ,
//3687607 ,
//3689454 ,
//3692956 ,
//3694616 ,
//3703404 ,
                              };
                             
            



            //int n = 1;
            //foreach(int SelectedInvoices in Invoices)
            //{   
            //ServicesInitializer majorinvoice = new ServicesInitializer();
            invService.InvoiceServiceClient service = CreateInvoiceServiceClient();
           
            foreach (int Inv in Invoices)

            {
                
                invService.DataFetchServiceResponseUsingInvoice invfetch = service.InvoiceFetchByInvoiceID(Convert.ToString(Inv));
                invfetch.Items[0].InvoiceGeneralInfo.ReprintOrReSubmitClaim = true;
                
            }
            invService.DataWriteMultipleServiceResponseOfInvoiceResubmissionResponseHEFRM8Lb InvResponse = service.ResubmitInvoices(Invoices, false);
            
            //if (InvResponse.Success != true)
                //{
                //   // activesheet.Range["A" + n].Value = SelectedInvoice;
                //    activesheet.Range["B" + n].Value = InvResponse.Messages[0];
                //}
                //else
                //{
                //   // activesheet.Range["A" + n].Value = SelectedInvoice;
                //    activesheet.Range["B" + n].Value = "Success";
                //}
                //n++;

            
            Console.WriteLine("Complete");
            Console.ReadLine();
            return;
        }
       
    }
}
