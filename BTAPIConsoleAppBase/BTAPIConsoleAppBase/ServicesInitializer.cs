﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace BTAPIConsoleAppBase
{
    class ServicesInitializer
    {
        public static soService.SalesOrderServiceClient soAction = ServicesInitializer.CreateSalesOrderServiceClient();
        public static ptService.PatientServiceClient ptAction = ServicesInitializer.CreatePatientServiceClient();
        public static docService.DocumentationServiceClient docAction = ServicesInitializer.CreateDocumentationServiceClient();
        public static invService.InvoiceServiceClient invAction = ServicesInitializer.CreateInvoiceServiceClient();


        public static soService.SalesOrderServiceClient CreateSalesOrderServiceClient()
        {



            BasicHttpBinding myBinding = new BasicHttpBinding();
            myBinding.MaxReceivedMessageSize = 65536 * 2;
            myBinding.Security.Mode = BasicHttpSecurityMode.Transport;
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress ea = new EndpointAddress("https://webservices.brightree.net/v0100-1507/OrderEntryService/SalesOrderService.svc");
            soService.SalesOrderServiceClient client = new soService.SalesOrderServiceClient();
            client.ClientCredentials.UserName.UserName = PublicVars.UserID;
            client.ClientCredentials.UserName.Password = PublicVars.UserPassword;

            return client;

        }

        public static ptService.PatientServiceClient CreatePatientServiceClient()
        {
            BasicHttpBinding myBinding = new BasicHttpBinding();
            myBinding.MaxReceivedMessageSize = 65536 * 2;
            myBinding.Security.Mode = BasicHttpSecurityMode.Transport;
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress ea = new EndpointAddress("https://webservices.brightree.net/v0100-1505/OrderEntryService/patientservice.svc");
            ptService.PatientServiceClient client = new ptService.PatientServiceClient();
            client.ClientCredentials.UserName.UserName = PublicVars.UserID;
            client.ClientCredentials.UserName.Password = PublicVars.UserPassword;
            return client;
        }

        public static docService.DocumentationServiceClient CreateDocumentationServiceClient()
        {
            BasicHttpBinding myBinding = new BasicHttpBinding();
            myBinding.MaxReceivedMessageSize = 65536 * 2;
            myBinding.Security.Mode = BasicHttpSecurityMode.Transport;
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress ea = new EndpointAddress("https://webservices.brightree.net/V0100-1505/DocumentationService/DocumentationService.svc");
            docService.DocumentationServiceClient client = new docService.DocumentationServiceClient();
            client.ClientCredentials.UserName.UserName = PublicVars.UserID;
            client.ClientCredentials.UserName.Password = PublicVars.UserPassword;
            return client;
        }

        public static insService.InsuranceServiceClient CreateInsuranceServiceClient()
        {
            BasicHttpBinding myBinding = new BasicHttpBinding();
            myBinding.MaxReceivedMessageSize = 65536 * 2;
            myBinding.Security.Mode = BasicHttpSecurityMode.Transport;
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress ea = new EndpointAddress("https://webservices.brightree.net/v0100-1210/OrderEntryService/InsuranceService.svc");
            insService.InsuranceServiceClient client = new insService.InsuranceServiceClient();
            client.ClientCredentials.UserName.UserName = PublicVars.UserID;
            client.ClientCredentials.UserName.Password = PublicVars.UserPassword;
            return client;
        }

        public static cfService.CustomFieldServiceClient CreateCustomFieldServiceClient()
        {
            BasicHttpBinding myBinding = new BasicHttpBinding();
            myBinding.MaxReceivedMessageSize = 65536 * 2;
            myBinding.Security.Mode = BasicHttpSecurityMode.Transport;
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress ea = new EndpointAddress("https://webservices.brightree.net/v0100/CustomFieldService/CustomFieldService.svc");
            cfService.CustomFieldServiceClient client = new cfService.CustomFieldServiceClient();
            client.ClientCredentials.UserName.UserName = PublicVars.UserID;
            client.ClientCredentials.UserName.Password = PublicVars.UserPassword;
            return client;
        }

        public static invService.InvoiceServiceClient CreateInvoiceServiceClient()
        {
            BasicHttpBinding myBinding = new BasicHttpBinding();
            myBinding.MaxReceivedMessageSize = 65536 * 2;
            myBinding.Security.Mode = BasicHttpSecurityMode.Transport;
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress ea = new EndpointAddress("https://webservices.brightree.net/v0100-1307/InvoiceService/InvoiceService.svc");
            invService.InvoiceServiceClient client = new invService.InvoiceServiceClient();
            client.ClientCredentials.UserName.UserName = PublicVars.UserID;
            client.ClientCredentials.UserName.Password = PublicVars.UserPassword;
            return client;
        }


    }
}
